CREATE TABLE IF NOT EXISTS questions (
    id INT NOT NULL PRIMARY KEY,
    question VARCHAR (255),
    option_a TEXT,
    option_b TEXT,
    option_c TEXT,
    option_d TEXT,
    book_id INT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    CONSTRAINT fk_book_id FOREIGN KEY(book_id) REFERENCES book(id)
);