CREATE TABLE IF NOT EXISTS genre (
    id INT NOT NULL PRIMARY KEY,
    title VARCHAR (255),
    code TEXT,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS book (
    id INT NOT NULL PRIMARY KEY,
    title VARCHAR (255),
    genre_id INT,
    book_image TEXT,
    soon VARCHAR(255),
    isNew BOOLEAN,
    created_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_at TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    deleted_at TIMESTAMP,
    CONSTRAINT fk_genre_id FOREIGN KEY(genre_id) REFERENCES genre(id)
);