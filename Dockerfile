# workspace (GOPATH) configured at /go
FROM golang:1.14 as builder


#
RUN mkdir -p $GOPATH/src/gitlab.com/quizlab/book_service
WORKDIR $GOPATH/src/gitlab.com/quizlab/book_service

# Copy the local package files to the container's workspace.
COPY . ./

# installing depends and build
RUN export CGO_ENABLED=0 && \
    export GOOS=linux && \
    make build && \
    mv ./bin/book_service /



FROM alpine
COPY --from=builder book_service .
ENTRYPOINT ["/book_service"]