package grpcclient

import (
	"fmt"

	"gitlab.com/quizlab/book_service/config"
	"google.golang.org/grpc"

	notificationPb "gitlab.com/quizlab/book_service/genproto/notification_service"
)

//IServiceManager ...
type IServiceManager interface {
	// PostService() postPb.PostServiceClient
	NotificationService() notificationPb.NotificationServiceClient
}

type serviceManager struct {
	cfg config.Config
	notificationService notificationPb.NotificationServiceClient
	// postService postPb.PostServiceClient
}

//New ...
func New(cfg config.Config) (IServiceManager, error) {
	// connPost, err := grpc.Dial(
	// 	fmt.Sprintf("%s:%d", cfg.PostServiceHost, cfg.PostServicePort),
	// 	grpc.WithInsecure())
	// if err != nil {
	// 	return nil, fmt.Errorf("post service dial host: %s port: %d",
	// 		cfg.PostServiceHost, cfg.PostServicePort)
	// }
	connNotification, err := grpc.Dial(
		fmt.Sprintf("%s:%d", cfg.NotificationServiceHost, cfg.NotificationServicePort),
		grpc.WithInsecure())
	if err != nil {
		return nil, fmt.Errorf("post service dial host: %s port: %d",
			cfg.NotificationServiceHost, cfg.NotificationServicePort)
	}


	serviceManager := &serviceManager{
		cfg: cfg,
		notificationService: notificationPb.NewNotificationServiceClient(connNotification),
		// postService: postPb.NewPostServiceClient(connPost),
	}

	return serviceManager, nil
}

// NotificationService ...
func (s *serviceManager) NotificationService() notificationPb.NotificationServiceClient {
	return s.notificationService
}

// // PostService ...
// func (s *serviceManager) PostService() postPb.PostServiceClient {
// 	return s.postService
// }
