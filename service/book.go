package service

import (
	"context"
	"database/sql"

	"github.com/jmoiron/sqlx"
	l "gitlab.com/quizlab/book_service/pkg/logger"
	grpcclient "gitlab.com/quizlab/book_service/service/grpc_client"
	"gitlab.com/quizlab/book_service/storage"
	"google.golang.org/grpc/codes"
	pb "gitlab.com/quizlab/book_service/genproto/book_service"
	"google.golang.org/grpc/status"
)

// BookService ...
type BookService struct {
	storage storage.I
	client  grpcclient.IServiceManager
	logger  l.Logger
}

// NewBookService ...
func NewBookService(db *sqlx.DB, log l.Logger, client grpcclient.IServiceManager) *BookService {
	return &BookService{
		storage: storage.NewStoragePg(db),
		logger:  log,
		client:  client,
	}
}

// GetAllBooks is a method for getting books))
func (s *BookService) GetAllBooks(ctx context.Context, req *pb.GetAllBooksRequest) (*pb.GetAllBooksResponse, error) {
	var (
		err      error
		requests []*pb.BookRes
		count    uint64
	)

	requests, count, err = s.storage.Book().GetAllBooks(req.GetUserId(), req.GetLimit(), req.GetPage())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting books, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting books", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllBooksResponse{
		Books: requests,
		Count: count,
	}, nil
}

// GetAllSoonBooks is a method for getting books))
func (s *BookService) GetAllSoonBooks(ctx context.Context, req *pb.GetAllBooksRequest) (*pb.GetAllBooksResponse, error) {
	var (
		err      error
		requests []*pb.BookRes
		count    uint64
	)

	requests, count, err = s.storage.Book().GetAllSoonBooks(req.GetUserId(), req.GetLimit(), req.GetPage())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting books, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting books", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllBooksResponse{
		Books: requests,
		Count: count,
	}, nil
}

// GetBooksByGenre is a method for getting books))
func (s *BookService) GetBooksByGenre(ctx context.Context, req *pb.GetBooksByGenreRequest) (*pb.GetBooksByGenreResponse, error) {
	var (
		err      error
		requests []*pb.BookRes
		count    int64
	)

	requests, count, err = s.storage.Book().GetBooksByGenre(req.GetGenreID(), req.GetLimit(), req.GetPage())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting books, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting books", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetBooksByGenreResponse{
		Books: requests,
		Count: count,
	}, nil
}

// GetGenres is a method for getting genres))
func (s *BookService) GetGenres(ctx context.Context, req *pb.GetGenresRequest) (*pb.GetGenresResponse, error) {
	var (
		err      error
		requests []*pb.GenreRes
		count    int64
	)

	requests, count, err = s.storage.Book().GetGenres(req.GetLimit(), req.GetPage())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting genres, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting genres", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetGenresResponse{
		Genres: requests,
		Count: count,
	}, nil
}

// GetAllQuestionByBook is a method for getting questions))
func (s *BookService) GetAllQuestionByBook(ctx context.Context, req *pb.GetAllQuestionByBookRequest) (*pb.GetAllQuestionByBookResponse, error) {
	var (
		err      error
		requests []*pb.Question
		count    uint64
	)

	requests, count, err = s.storage.Book().GetAllQuestionsByBook(req.GetBookId())
	if err == sql.ErrNoRows {
		s.logger.Error("Error while getting questions, Not Found", l.Any("req", req))
		return nil, status.Error(codes.NotFound, "Not found")
	} else if err != nil {
		s.logger.Error("Error while getting questions", l.Error(err), l.Any("req", req))
		return nil, status.Error(codes.Internal, "Internal server error")
	}

	return &pb.GetAllQuestionByBookResponse{
		Questions: requests,
		Count: count,
	}, nil
}
