package postgres

import (
	"database/sql"

	"github.com/jmoiron/sqlx"
	pb "gitlab.com/quizlab/book_service/genproto/book_service"
	"gitlab.com/quizlab/book_service/storage/repo"
)

type bookRepo struct {
	db *sqlx.DB
}

// NewBookRepo ...
func NewBookRepo(db *sqlx.DB) repo.BookStorageI {
	return &bookRepo{
		db: db,
	}
}

func (nr *bookRepo) GetAllBooks(userID string, limit, page uint64) ([]*pb.BookRes, uint64, error) {
	var (
		requests      []*pb.BookRes
		count, offset uint64
	)

	offset = (page - 1) * limit

	rows, err := nr.db.Query(`
	SELECT
			id,
	   		title,
			genre_id,
			book_image,
			soon,
			isnew,
			created_at,
			updated_at
	FROM book
		WHERE deleted_at IS NULL AND soon='false' ORDER BY created_at DESC LIMIT $1 OFFSET $2`, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var book pb.BookRes
		if err := rows.Scan(
			&book.Id,
			&book.Title,
			&book.GenreId,
			&book.BookImage,
			&book.Soon,
			&book.IsNew,
			&book.CreatedAt,
			&book.UpdatedAt,
		); err != nil {
			return nil, 0, err
		}
		requests = append(requests, &book)
	}

	count, err = nr.GetBooksCount()
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (nr *bookRepo) GetBooksCount() (uint64, error) {
	var count uint64

	row := nr.db.QueryRow(`
 		SELECT count(*)
		FROM book 
		WHERE deleted_at IS NULL AND soon='false' 
 	`)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

func (nr *bookRepo) GetAllSoonBooks(userID string, limit, page uint64) ([]*pb.BookRes, uint64, error) {
	var (
		requests      []*pb.BookRes
		count, offset uint64
	)

	offset = (page - 1) * limit

	rows, err := nr.db.Query(`
	SELECT
			id,
	   		title,
			genre_id,
			book_image,
			soon,
			isnew,
			created_at,
			updated_at
	FROM book
		WHERE deleted_at IS NULL AND soon='true' ORDER BY created_at DESC LIMIT $1 OFFSET $2`, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var book pb.BookRes
		if err := rows.Scan(
			&book.Id,
			&book.Title,
			&book.GenreId,
			&book.BookImage,
			&book.Soon,
			&book.IsNew,
			&book.CreatedAt,
			&book.UpdatedAt,
		); err != nil {
			return nil, 0, err
		}
		requests = append(requests, &book)
	}

	count, err = nr.GetSoonBooksCount()
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (nr *bookRepo) GetSoonBooksCount() (uint64, error) {
	var count uint64

	row := nr.db.QueryRow(`
 		SELECT count(*)
		FROM book 
		WHERE deleted_at IS NULL AND soon='true' 
 	`)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

func (nr *bookRepo) GetGenres(limit, page int64) ([]*pb.GenreRes, int64, error) {
	var (
		requests      []*pb.GenreRes
		count, offset int64
	)

	offset = (page - 1) * limit

	rows, err := nr.db.Query(`
	SELECT
			id,
	   		title,
			code
	FROM genre
		WHERE deleted_at IS NULL ORDER BY created_at DESC LIMIT $1 OFFSET $2`, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var gr pb.GenreRes
		if err := rows.Scan(
			&gr.Id,
			&gr.Title,
			&gr.Code,
		); err != nil {
			return nil, 0, err
		}
		requests = append(requests, &gr)
	}

	count, err = nr.GetGenresCount()
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (nr *bookRepo) GetGenresCount() (int64, error) {
	var count int64

	row := nr.db.QueryRow(`
 		SELECT count(*)
		FROM genre 
		WHERE deleted_at IS NULL 
 	`)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

func (nr *bookRepo) GetAllQuestionsByBook(bookID string) ([]*pb.Question, uint64, error) {
	var (
		requests []*pb.Question
		count    uint64
		optionD sql.NullString
	)

	rows, err := nr.db.Query(`
		SELECT
			id,
       		question,
			option_a,
			option_b,
			option_c,
			option_d,
			book_id
		FROM questions 
		WHERE book_id=$1`, bookID)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var qs pb.Question
		if err := rows.Scan(
			&qs.Id,
			&qs.Question,
			&qs.OptionA,
			&qs.OptionB,
			&qs.OptionC,
			&optionD,
			&qs.BookId,
		); err != nil {
			return nil, 0, err
		}
		qs.OptionD = optionD.String
		requests = append(requests, &qs)
	}

	count, err = nr.GetQuestionsCountByBook(bookID)
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (nr *bookRepo) GetQuestionsCountByBook(id string) (uint64, error) {
	var count uint64

	row := nr.db.QueryRow(`
 		SELECT count(*)
		FROM questions 
		WHERE book_id=$1
 	`, id)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}

func (nr *bookRepo) GetBooksByGenre(genreID, limit, page int64) ([]*pb.BookRes, int64, error) {
	var (
		requests      []*pb.BookRes
		count, offset int64
	)

	offset = (page - 1) * limit

	rows, err := nr.db.Query(`
	SELECT
			id,
	   		title,
			genre_id,
			book_image,
			soon,
			isnew,
			created_at,
			updated_at
	FROM book
		WHERE deleted_at IS NULL AND soon='false' AND genre_id=$1 ORDER BY created_at DESC LIMIT $2 OFFSET $3`, genreID, limit, offset)

	if err != nil {
		return nil, 0, err
	}

	for rows.Next() {
		var book pb.BookRes
		if err := rows.Scan(
			&book.Id,
			&book.Title,
			&book.GenreId,
			&book.BookImage,
			&book.Soon,
			&book.IsNew,
			&book.CreatedAt,
			&book.UpdatedAt,
		); err != nil {
			return nil, 0, err
		}
		requests = append(requests, &book)
	}

	count, err = nr.GetBooksByGenreCount(genreID)
	if err != nil {
		return nil, 0, err
	}

	return requests, count, nil
}

func (nr *bookRepo) GetBooksByGenreCount(genreID int64) (int64, error) {
	var count int64

	row := nr.db.QueryRow(`
 		SELECT count(*)
		FROM book 
		WHERE deleted_at IS NULL AND soon='false' and genre_id=$1
 	`, genreID)

	if err := row.Scan(&count); err != nil {
		return 0, err
	}

	return count, nil
}