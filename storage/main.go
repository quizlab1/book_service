package storage

import (
	"github.com/jmoiron/sqlx"
	"gitlab.com/quizlab/book_service/storage/postgres"
	"gitlab.com/quizlab/book_service/storage/repo"
)

// I is an interface for storage
type I interface {
	Book() repo.BookStorageI
}

type storagePg struct {
	db        *sqlx.DB
	bookRepo  repo.BookStorageI
}

// NewStoragePg ...
func NewStoragePg(db *sqlx.DB) I {
	return &storagePg{
		db:        db,
		bookRepo:  postgres.NewBookRepo(db),
	}
}

func (s storagePg) Book() repo.BookStorageI {
	return s.bookRepo
}
