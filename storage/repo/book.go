package repo

import (
	"errors"
	pb "gitlab.com/quizlab/book_service/genproto/book_service"
)

var (
	// ErrAlreadyExists ...
	ErrAlreadyExists = errors.New("Already exists")
	// ErrInvalidField ...
	ErrInvalidField = errors.New("Incorrect field")
)

// BookStorageI is an interface for client storage
type BookStorageI interface {
	GetAllBooks(userID string, limit, page uint64) ([]*pb.BookRes, uint64, error)
	GetAllSoonBooks(userID string, limit, page uint64) ([]*pb.BookRes, uint64, error)
	GetGenres(limit, page int64) ([]*pb.GenreRes, int64, error)
	GetBooksByGenre(genreID, limit, page int64) ([]*pb.BookRes, int64, error)
	GetAllQuestionsByBook(bookID string) ([]*pb.Question, uint64, error)
}
